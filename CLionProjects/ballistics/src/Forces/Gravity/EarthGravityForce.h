//
// Created by perseverance on 19.10.23.
//

#ifndef BALLISTICS_EARTHGRAVITYFORCE_H
#define BALLISTICS_EARTHGRAVITYFORCE_H

#include "types/Vector.h"
#include "Time/Time.h"
#include "GeographicLib/GravityModel.hpp"

namespace Ballistics::Force {

    class EarthGravityForce {
        GeographicLib::GravityModel model_;
    public:
        struct SatelliteParameters{};
        EarthGravityForce(const std::string &model,
                          const std::string &path,
                          int Nmax, int Mmax) : model_(model, path, Nmax, Mmax) {}

        Vector3d calcAccelerationEcef(const Vector3d &positionEcef) {
            Vector3d result;
            model_.V(positionEcef.x(), positionEcef.y(), positionEcef.z(), result.x(), result.y(), result.z());
            return result;
        }

        template<typename T>
        Vector3d calcForce(const Time<Scale::TT> &time,
                           const Vector3d &position,
                           const Vector3d &velocity,
                           const double /*mass*/,
                           const SatelliteParameters &SattelliteParameters,
                           const T &data) {
            return calcForce(time, position, velocity, data.sunPosition,
                             data.Velocity, data.moonPosition);
        }
    };
};

#endif //BALLISTICS_EARTHGRAVITYFORCE_H