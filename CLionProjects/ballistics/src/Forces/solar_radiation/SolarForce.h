//
// Created by perseverance on 22.11.23.
//

#ifndef BALLISTICS_SOLARFORCE_H
#define BALLISTICS_SOLARFORCE_H

#include "types/Vector.h"
#include "Time/Time.h"

namespace Ballistics::Force {

    template<typename SolarPressure, typename InteractionModel>
    class SolarForce {
        SolarPressure solarPressure_;

    public:

        using SattelliteParameters = InteractionModel::SatelliteParameters;

        Vector3d calcForce(const Time<Scale::TT> &time,
                           const Vector3d &position,
                           const Vector3d &velocity,
                           const Vector3d &sunPosition,
                           const Vector3d &sunVelocity,
                           const Vector3d &moonPosition,
                           const SattelliteParameters &SattelliteParameters) {

            const Vector3d solarPressure = solarPressure_.calcSolarPressure(time, position, velocity,
                                                                       sunPosition, sunVelocity, moonPosition);
            return InteractionModel::calcForce(solarPressure, SattelliteParameters);
        }

        template<typename T>
        Vector3d calcForce(const Time<Scale::TT> &time,
                           const Vector3d &position,
                           const Vector3d &velocity,
                           const double /*mass*/,
                           const SattelliteParameters &SattelliteParameters,
                           const T &data) {
            return calcForce(time, position, velocity, data.sunPosition,
                             data.Velocity, data.moonPosition);
        }
    };

}// namespace Ballistics::Force

#endif //BALLISTICS_SOLARFORCE_H