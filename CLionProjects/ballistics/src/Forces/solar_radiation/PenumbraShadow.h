//
// Created by perseverance on 16.11.23.
//

#ifndef BALLISTICS_PENUMBRASHADOW_H
#define BALLISTICS_PENUMBRASHADOW_H

#include "types/Vector.h"

/// Из цилиндрической конической и модели с полутенью модель с полутенью самая норм. Вот и она

namespace Ballistics::Force {

    class PenumbraShadow {

        double sunRadius_;
        double bodyRadius_;

    public:
        PenumbraShadow(double sunRadius, double bodyRadius) noexcept: sunRadius_(sunRadius), bodyRadius_(bodyRadius) {};

        [[nodiscard]] inline double calcShadow(const Vector3d &position, const Vector3d &bodyPosition,
                                 const Vector3d &sunPosition) const;
    };

    double PenumbraShadow::calcShadow(const Vector3d &position, const Vector3d &bodyPosition,
                                      const Vector3d &sunPosition) const {

        const Vector3d deltabr = bodyPosition - position;
        const Vector3d deltasr = sunPosition - position;

        //assert(bodyRadius_ / deltabr.norm() < 1);
        //assert(sunRadius_ / deltasr.norm() < 1);
        //assert(sunRadius_ + bodyRadius_ < std::abs(sunPosition.norm() - bodyPosition.norm()));


        const double thetta_b = std::asin(bodyRadius_ / deltabr.norm());
        const double thetta_s = std::asin(sunRadius_ / deltasr.norm());

        const double thetta_bs = std::acos(deltabr.dot(deltasr) /
                                             (deltabr.norm() * deltasr.norm()));



        const double s2 = thetta_s * thetta_s;
        const double b2 = thetta_b * thetta_b;
        const double bs2 = thetta_bs * thetta_bs;

        if (thetta_bs >= thetta_b + thetta_s) {
            return 1;
        } else if (thetta_bs < thetta_b - thetta_s) {
            return 0;
        } else if (thetta_bs < thetta_s - thetta_b) {
            return (1 - b2/s2);
        } else if (thetta_bs > std::abs(thetta_b - thetta_s) && thetta_bs < thetta_s + thetta_b) {
            const double caf = std::acos(
                    (s2 + bs2 - b2) / (2 * thetta_bs * thetta_s));
            const double cbd = std::acos(
                    (b2 + bs2 - s2) / (2 * thetta_b * thetta_bs));

            const double S_afc = 0.5 * caf * s2;
            const double S_aec = 0.5 * std::sin(caf) * std::cos(caf) * s2;
            const double S_bdc = 0.5 * cbd * b2;
            const double S_bec = 0.5 * std::sin(cbd) * std::cos(cbd) * b2;
            const double S = 2 * (S_afc - S_aec) + 2 * (S_bdc - S_bec);

            return (1 - S / (M_PI * s2));
        }
    };

}// namespace Ballistics::Force

#endif //BALLISTICS_PENUMBRASHADOW_H