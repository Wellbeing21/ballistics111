//
// Created by perseverance on 16.11.23.
//

#ifndef BALLISTICS_INDEPENDENTSHADOW_H
#define BALLISTICS_INDEPENDENTSHADOW_H

#include "types/Vector.h"

/// Мы в системе GCRS координаты земли по нулям

namespace Ballistics::Force {

    template<typename EarthShadow, typename MoonShadow>
    class IndependentShadow {
        EarthShadow earthShadow_;
        MoonShadow moonShadow_;

    public:
        IndependentShadow(const EarthShadow &earthShadow,
                          const MoonShadow &moonShadow) noexcept
                : earthShadow_(earthShadow), moonShadow_(moonShadow) {};


        [[nodiscard]] inline double result_shadow(const Vector3d &position,
                          const Vector3d &sunPosition,
                          const Vector3d &moonPosition)const noexcept{
            const double F = std::min(earthShadow_.calcShadow(position, Vector3d::Zero(), sunPosition),
                                      moonShadow_.calcShadow(position,  moonPosition, sunPosition));
            return  F;
        }
    };

}// namespace Ballistics::Force

#endif //BALLISTICS_INDEPENDENTSHADOW_H