//
// Created by perseverance on 23.11.23.
//

#ifndef BALLISTICS_SPHEREMODEL_H
#define BALLISTICS_SPHEREMODEL_H

#include "types/Vector.h"

namespace Ballistics::Force {

    class SphereModel {

    public:
        struct SatelliteParameters {
            double area;
        };

        /// инлайн: компилятор подставляет функцию в сам код, не вызывает ее. Нужно так делать
        /// если есть несколько реализаций такой функции и если функция маленькая
        inline static Vector3d calcForce(const Vector3d &solarPressure,
                                         const SatelliteParameters &satelliteParameters) noexcept {
            return solarPressure * satelliteParameters.area;


        }
    };
}

#endif //BALLISTICS_SPHEREMODEL_H