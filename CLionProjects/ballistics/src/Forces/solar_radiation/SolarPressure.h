//
// Created by perseverance on 16.11.23.
//

#ifndef BALLISTICS_SOLARPRESSURE_H
#define BALLISTICS_SOLARPRESSURE_H

#include "types/Vector.h"
#include "Time/Time.h"

namespace Ballistics::Force {

    template<typename TSIModel, typename ShadowModel>
    class SolarPressure {
        TSIModel tsiModel_;
        ShadowModel shadowModel_;
        constexpr static double AU = 149597870700;
        constexpr static double c = 300000000;

    public:
        SolarPressure(const TSIModel &tsiModel, const ShadowModel &shadowModel) {};

        /*** Считает поток энергии, деленый на скорость света ***/
        Vector3d calcSolarPressure(const Time<Scale::TT> &time,
                                   const Vector3d &position,
                                   const Vector3d &velocity,
                                   const Vector3d &sunPosition,
                                   const Vector3d &sunVelocity,
                                   const Vector3d &moonPosition) const noexcept;
    };

    template<typename TSIModel, typename ShadowModel>
    Vector3d SolarPressure<TSIModel, ShadowModel>::calcSolarPressure(const Time<Scale::TT> &time,
                                                                     const Vector3d &position,
                                                                     const Vector3d &velocity,
                                                                     const Vector3d &sunPosition,
                                                                     const Vector3d &sunVelocity,
                                                                     const Vector3d &moonPosition) const noexcept {
        const Vector3d n = (position - sunPosition) / (position - sunPosition).norm();
        const Vector3d Vrel = velocity - sunVelocity;

        const Vector3d vc = Vrel / c;
        const double norm = (position - sunPosition).norm();

        const Vector3d j0 = tsiModel_.TSI(time) * AU * AU * (n * (1 - n.dot(vc)) - vc)
                            / (norm * norm);
        return j0;
    }
}// namespace Ballistics::Force

#endif //BALLISTICS_SOLARPRESSURE_H