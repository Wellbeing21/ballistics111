//
// Created by perseverance on 16.11.23.
//

#ifndef BALLISTICS_CONSTANTSOLARIRRADIANCE_H
#define BALLISTICS_CONSTANTSOLARIRRADIANCE_H

#include "Time/Time.h"

namespace Ballistics::Force {

    class ConstantTSI{
        static constexpr double TSI_ = 1366;

    public:
        [[nodiscard]] constexpr double static TSI(const Time<Scale::TT> &tt) noexcept {return TSI_;}
    };
}

#endif //BALLISTICS_CONSTANTSOLARIRRADIANCE_H