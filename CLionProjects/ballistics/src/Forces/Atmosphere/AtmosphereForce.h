//
// Created by perseverance on 30.11.23.
//

#ifndef BALLISTICS_ATMOSPHEREFORCE_H
#define BALLISTICS_ATMOSPHEREFORCE_H

#include "4401CalcDensity.h"
#include "../solar_radiation/SphereModel.h"

namespace Ballistics::Atmosphere {

    template<typename AtmosphereModel, typename InteractionModel>
    class AtmosphereForce {
    private:
        const double Cd_;
        const double height_;
        const Vector3d velocity_;
    public:
        AtmosphereForce(double Cd, double height, const Vector3d &velocity) noexcept: Cd_(Cd), velocity_(velocity),
                                                                                      height_(height) {};

        [[nodiscard]] inline Vector3d calcForce() const noexcept {
            Vector3d atmosphere_pressure;

            double abs_force = 0.5 * AtmosphereModel::calc_density(height_) * velocity_.dot(velocity_) * Cd_; ///как правильно достать площадь?
            atmosphere_pressure = - velocity_ / velocity_.norm() * abs_force;

            return atmosphere_pressure;
        };
    };
}
#endif //BALLISTICS_ATMOSPHEREFORCE_H