//
// Created by perseverance on 09.11.23.
//

#ifndef BALLISTICS_4401CALCDENSITY_H
#define BALLISTICS_4401CALCDENSITY_H

#include "Gost4401.h"


namespace Ballistcs::Atmosphere {

    double calc_density(double height) noexcept {
        GOST4401_81Raw Gost4401;

        const double *it = std::upper_bound(Gost4401.height.begin(),
                                            Gost4401.height.end(), height);

        /// находим номер элемента, который больше вход. аргумента height
        int count = 0;
        while (Gost4401.height[count] != *it) {
            ++count;
        }

        /// типо интерполятор
        double alpha = (height - Gost4401.height[count - 1]) / (Gost4401.height[count] - Gost4401.height[count - 1]);
        double dens = alpha * (Gost4401.density[count] - Gost4401.density[count - 1]) + Gost4401.density[count - 1];

        return dens;
    }

}
#endif //BALLISTICS_4401CALCDENSITY_H