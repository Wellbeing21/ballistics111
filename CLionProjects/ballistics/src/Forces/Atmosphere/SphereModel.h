//
// Created by perseverance on 12.12.23.
//

#ifndef BALLISTICS_SPHEREMODEL_H
#define BALLISTICS_SPHEREMODEL_H
#include "types/Vector.h"


namespace Ballistics::Atmosphere {

    class SphereModel {

    public:
        struct SatelliteParameters {
            double area;
        };

        /// инлайн: компилятор подставляет функцию в сам код, не вызывает ее. Нужно так делать
        /// если есть несколько реализаций такой функции и если функция маленькая
        inline static Vector3d calcForce(const Vector3d & atmPressure,
                                         const SatelliteParameters &satelliteParameters) noexcept {
            return atmPressure * satelliteParameters.area;
        }
    };
}

#endif //BALLISTICS_SPHEREMODEL_H