//
// Created by perseverance on 19.10.23.
//

#ifndef BALLISTICS_VECTOR_H
#define BALLISTICS_VECTOR_H

#include "Eigen/Core"

namespace Ballistics {

    template<typename T>
    using Vector3 = Eigen::Vector3<T>;

    using Vector3d = Vector3<double>;
}

#endif //BALLISTICS_VECTOR_H