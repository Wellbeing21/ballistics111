//
// Created by perseverance on 19.10.23.
//

#ifndef BALLISTICS_ACCELERATIONS_GRAPH_H
#define BALLISTICS_ACCELERATIONS_GRAPH_H
#include "types/Vector.h"
#include <cmath>
#include <vector>

// Generate vector with equidistant points
std::vector<double> linspace();

// Sphere structure
struct sphere_cont;

// Generate sphere by equidistant points
sphere_cont sphere();

// |Vector_1 - Vector_2|
double metric();

#endif //BALLISTICS_ACCELERATIONS_GRAPH_H
