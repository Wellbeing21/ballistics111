//
// Created by perseverance on 19.10.23.
//
#include "delta_acceleration.h"
#include "accelerations_graph.cpp"
#include "accelerations_graph.h"


double
calc_lg_L(const std::string model, const std::string path, const double Radius, const double count, const double Nmax1,
          const double Mmax1,
          const double Nmax2, const double Mmax2) noexcept {

    // Create two models with different amount of harmonics
    Ballistics::Force::EarthGravityForce model1(model,
                                                path,
                                                Nmax1, Mmax1);

    Ballistics::Force::EarthGravityForce model2(model,
                                                path,
                                                Nmax2, Mmax2);

    // Create std::vector of geometric vectors (eigen)
    std::vector<Ballistics::Vector3d> xyz;
    xyz.resize(count);

    // Create sphere with radius R with count amount of points
    sphere_cont SPHERE = sphere(Radius, count);
    for (int i = 0; i < count; i++) {
        xyz[i].x() = SPHERE.x[i];
        xyz[i].y() = SPHERE.y[i];
        xyz[i].z() = SPHERE.z[i];
    }

    // Create std::vector of geometric vectors of accelerations (eigen) for models
    std::vector<Ballistics::Vector3d> xyz_acc_model1;
    xyz_acc_model1.resize(count);
    for (int i = 0; i < xyz_acc_model1.size(); i++) {
        xyz_acc_model1[i] = model1.calcAccelerationEcef(xyz[i]);
    }

    std::vector<Ballistics::Vector3d> xyz_acc_model2;
    xyz_acc_model2.resize(count);
    for (int i = 0; i < xyz_acc_model2.size(); i++) {
        xyz_acc_model2[i] = model2.calcAccelerationEcef(xyz[i]);
    }

    // Calculate L;
    double L = 0;
    for (int i = 0; i < xyz_acc_model1.size(); i++) {
        L += metric(xyz_acc_model1[i], xyz_acc_model2[i]);
        //L += metric(xyz_acc_model1[i], xyz_acc_model2[i]) <= 1e-16 ? L += metric(xyz_acc_model1[i], xyz_acc_model2[i]): L = 1e-16;
        //L >= 1e-16 ? L += metric(xyz_acc_model1[i], xyz_acc_model2[i]) : L = 1e-16;
    }
    L = L / count;

    return log10(L);
}