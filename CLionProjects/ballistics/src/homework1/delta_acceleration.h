//
// Created by perseverance on 19.10.23.
//

#ifndef BALLISTICS_DELTA_ACCELERATION_H
#define BALLISTICS_DELTA_ACCELERATION_H
#include "Forces/Gravity/EarthGravityForce.h"
#include "homework1/accelerations_graph.h"
#include "accelerations_graph.h"

double calc_lg_L();

#endif //BALLISTICS_DELTA_ACCELERATION_H