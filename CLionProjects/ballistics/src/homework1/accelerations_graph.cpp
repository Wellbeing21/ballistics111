//
// Created by perseverance on 19.10.23.
//

#include "accelerations_graph.h"

struct sphere_cont {
    std::vector<double> x;
    std::vector<double> y;
    std::vector<double> z;
};

std::vector<double> linspace(const double start, const double end, double count) noexcept {
    std::vector<double> r;
    r.reserve(count);
    double step = (end - start) / (count - 1); //(count - 1) ???
    for (int i = 0; i < count; i++) {
        r.push_back(start + i * step);
    }
    return r;
}

sphere_cont sphere(double Radius, double count) noexcept {
    sphere_cont sphere_;
    sphere_.x.resize(count);
    sphere_.y.resize(count);
    sphere_.z.resize(count);

    std::vector<double> phi = linspace(0, 2 * M_PI, count);
    std::vector<double> thetta = linspace(0, M_PI, count);

    for (int i = 0; i < count; ++i) {
        sphere_.x[i] = Radius * sin(thetta[i]) * cos(phi[i]);
        sphere_.y[i] = Radius * sin(thetta[i]) * sin(phi[i]);
        sphere_.z[i] = Radius * cos(thetta[i]);
    }

    return sphere_;
}

double metric(const Ballistics::Vector3d &acc1, const Ballistics::Vector3d &acc2) noexcept {
    double metric_ = 0;
    metric_ += sqrt((acc1.x() - acc2.x()) * (acc1.x() - acc2.x()) + (acc1.y() - acc2.y()) * (acc1.y() - acc2.y()) +
                  (acc1.z() - acc2.z()) * (acc1.z() - acc2.z()));
    return metric_;
};