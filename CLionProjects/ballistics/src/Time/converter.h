//
// Created by perseverance on 30.09.23.
//

#ifndef PROJECT_B_CONVERTER_H
#define PROJECT_B_CONVERTER_H

#include "Time.h"
#include "dut_container.h"
#include <cmath>
#include <cassert>
#include "GeographicLib/Accumulator.hpp"

template<typename Dut_container>
class TimeConverter {
private:
    Dut_container dutContainer_;

    using UT1 = Time<Scale::UT1>;
    using UTC = Time<Scale::UTC>;
    using TAI = Time<Scale::TAI>;
    using TT  = Time<Scale::TT> ;
    using TCG = Time<Scale::TCG>;
    using TCB = Time<Scale::TCB>;
    using TDB = Time<Scale::TDB>;

public:
    TimeConverter(const Dut_container &dutContainer) noexcept: dutContainer_(dutContainer) {};

    [[nodiscard]] TT TAI_TT(const TAI &Tai) const noexcept;
    [[nodiscard]] TT UT1_TT(const UT1 &Ut1) const noexcept;
    [[nodiscard]] TT UTC_TT(const UTC &Utc) const noexcept;
    [[nodiscard]] TT TCG_TT(const TCG &Tcg) const noexcept;
    [[nodiscard]] TT TDB_TT(const TDB &Tdb) const noexcept;
    [[nodiscard]] TT TCB_TT(const TCB &Tcb) const noexcept;

    [[nodiscard]] TAI TT_TAI (const TT  & Tt) const noexcept;
    [[nodiscard]] TAI UTC_TAI(const UTC &Utc) const noexcept;
    [[nodiscard]] TAI UT1_TAI(const UT1 &UT1) const noexcept;
    [[nodiscard]] TAI TCG_TAI(const TCG &Tcg) const noexcept;
    [[nodiscard]] TAI TCB_TAI(const TCB &Tcb) const noexcept;
    [[nodiscard]] TAI TDB_TAI(const TDB &Tdb) const noexcept;

    [[nodiscard]] UTC UT1_UTC(const UT1 &Ut1) const noexcept;
    [[nodiscard]] UTC TAI_UTС(const TAI &Tai) const noexcept;
    [[nodiscard]] UTC TT_UTC (const TT  &Tt ) const noexcept;
    [[nodiscard]] UTC TCG_UTC(const TCG &Tcg) const noexcept;
    [[nodiscard]] UTC TCB_UTC(const TCB &Tcb) const noexcept;
    [[nodiscard]] UTC TDB_UTC(const TDB &Tdb) const noexcept;

    /** @brief bla
     *
     * @param Tai
     * @return 11
     */
    [[nodiscard]] UT1 TAI_UT1(const TAI &Tai) const noexcept;

    /**
     *
     * @param Utc Время
     * @return Ut1
     */
    [[nodiscard]] UT1 UTC_UT1(const UTC &Utc) const noexcept;
    [[nodiscard]] UT1 TT_UT1 (const TT  &Tt ) const noexcept;
    [[nodiscard]] UT1 TCG_UT1(const TCG &Tcg) const noexcept;
    [[nodiscard]] UT1 TDB_UT1(const TDB &Tdb) const noexcept;
    [[nodiscard]] UT1 TCB_UT1(const TCB &Tcb) const noexcept;

    [[nodiscard]] TCG TT_TCG (const TT  & Tt) const noexcept;
    [[nodiscard]] TCG TDB_TCG(const TDB &Tdb) const noexcept;
    [[nodiscard]] TCG TCB_TCG(const TCB &Tcb) const noexcept;
    [[nodiscard]] TCG TAI_TCG(const TAI &Tai) const noexcept;
    [[nodiscard]] TCG UTC_TCG(const UTC &Utc) const noexcept;
    [[nodiscard]] TCG UT1_TCG(const UT1 &Ut1) const noexcept;

    [[nodiscard]] TDB TT_TDB (const TT  & Tt) const noexcept;
    [[nodiscard]] TDB TCB_TDB(const TCB &Tcb) const noexcept;
    [[nodiscard]] TDB TCG_TDB(const TCG &Tcg) const noexcept;
    [[nodiscard]] TDB TAI_TDB(const TAI &Tai) const noexcept;
    [[nodiscard]] TDB UTC_TDB(const UTC &Utc) const noexcept;
    [[nodiscard]] TDB UT1_TDB(const UT1 &Ut1) const noexcept;

    [[nodiscard]] TCB TDB_TCB(const TDB &Tdb) const noexcept;
    [[nodiscard]] TCB TT_TCB (const TT  & Tt) const noexcept;
    [[nodiscard]] TCB TCG_TCB(const TCG &Tcg) const noexcept;
    [[nodiscard]] TCB TAI_TCB(const TAI &Tai) const noexcept;
    [[nodiscard]] TCB UTC_TCB(const UTC &Utc) const noexcept;
    [[nodiscard]] TCB UT1_TCB(const UT1 &Ut1) const noexcept;

    double dtr(const double tt) const noexcept;

    template<Scale To, Scale From>
    [[nodiscard]] Time<To> convert(const Time<From> &from) const {
        Time<From> time;
        if constexpr (To == Scale::TT && From == Scale::TAI) {
            return TAI_to_TT(time);
        };
    }
};

template<typename Dut_container>
double TimeConverter<Dut_container>::dtr(const double tt) const noexcept {
    return 0.001657 * std::sin(6.24 + 0.017202 * tt - 2451545);
}

//---------------------------------------------------------------------------------------------------//
//------------------------------------------CONVERSIONS----------------------------------------------//
//---------------------------------------------------------------------------------------------------//

//---------------------------//
//-------------TT------------//
//---------------------------//

template<typename Dut_container>
Time<Scale::TT> TimeConverter<Dut_container>::TAI_TT(const Time<Scale::TAI> &Tai) const noexcept {
    double tt1;
    double tt2;
    const int func_value = iauTaitt(Tai.jdInt(), Tai.jdFrac(), &tt1, &tt2);
    assert(func_value == -1);
    return {tt1, tt2};
}

template<typename Dut_container>
Time<Scale::TT> TimeConverter<Dut_container>::UT1_TT(const Time<Scale::UT1> &Ut1) const noexcept {
    Time<Scale::UTC> timeUTC = UT1_UTC(Ut1);
    return UTC_TT(timeUTC);
}

template<typename Dut_container>
Time<Scale::TT> TimeConverter<Dut_container>::UTC_TT(const Time<Scale::UTC> &UtC) const noexcept {
    Time<Scale::TAI> timeTAI = UTC_TAI(UtC);
    return TAI_TT(timeTAI);
}

template<typename Dut_container>
Time<Scale::TT> TimeConverter<Dut_container>::TCG_TT(const TimeConverter::TCG &Tcg) const noexcept {
    double tt1;
    double tt2;
    int func_value = iauTcgtt(Tcg.jdInt(), Tcg.jdFrac(), &tt1, &tt2);
    assert(func_value == -1);
    return {tt1, tt2};
}

template<typename Dut_container>
Time<Scale::TT> TimeConverter<Dut_container>::TDB_TT(const TimeConverter::TDB &Tdb) const noexcept {
    double tt1;
    double tt2;
    int func_value = iauTdbtt(Tdb.jdInt(), Tdb.jdFrac(), dtr(Tdb.jd()), &tt1, &tt2);
    return {tt1, tt2};
}

template<typename Dut_container>
Time<Scale::TT> TimeConverter<Dut_container>::TCB_TT(const TimeConverter::TCB &Tcb) const noexcept {
    Time<Scale::TDB> timeTDB = TCB_TDB(Tcb);
    return TDB_TT(timeTDB);
}


//----------------------------//
//------------TAI-------------//
//----------------------------//

template<typename Dut_container>
Time<Scale::TAI> TimeConverter<Dut_container>::TT_TAI(const Time<Scale::TT> &Tt) const noexcept {
    double tai1;
    double tai2;
    int func_value = iauTttai(Tt.jdInt(), Tt.jdFrac(), &tai1, &tai2);
    assert(func_value == 0);
    return {tai1, tai2};
}

template<typename Dut_container>
Time<Scale::TAI> TimeConverter<Dut_container>::UTC_TAI(const Time<Scale::UTC> &Utc) const noexcept {
    double tai1;
    double tai2;
    int func_value = iauUtctai(Utc.jdInt(), Utc.jdFrac(), &tai1, &tai2);
    assert(func_value == -1);
    return {tai1, tai2};
}

template<typename Dut_container>
Time<Scale::TAI> TimeConverter<Dut_container>::UT1_TAI(const Time<Scale::UT1> &Ut1) const noexcept {
    Time<Scale::TT> timeTT = UT1_TT(Ut1);
    return TT_TAI(timeTT);
}

template<typename Dut_container>
Time<Scale::TAI> TimeConverter<Dut_container>::TCG_TAI(const TimeConverter::TCG &Tcg) const noexcept {
    Time<Scale::TT> timeTT = TCG_TT(Tcg);
    return TT_TAI(timeTT);
}

template<typename Dut_container>
Time<Scale::TAI> TimeConverter<Dut_container>::TCB_TAI(const TimeConverter::TCB &Tcb) const noexcept {
    Time<Scale::TT> timeTT = TCB_TT(Tcb);
    return TT_TAI(timeTT);
}

template<typename Dut_container>
Time<Scale::TAI> TimeConverter<Dut_container>::TDB_TAI(const TimeConverter::TDB &Tdb) const noexcept {
    Time<Scale::TT> timeTT = TDB_TT(Tdb);
    return TT_TAI(timeTT);
}

//----------------------------//
//------------UTC-------------//
//----------------------------//

template<typename Dut_container>
Time<Scale::UTC> TimeConverter<Dut_container>::TAI_UTС(const Time<Scale::TAI> &Tai) const noexcept {
    double utc1;
    double utc2;
    int func_value = iauTaiutc(Tai.jdInt(), Tai.jdFrac(), &utc1, &utc2);
    assert(func_value == -1);
    return {utc1, utc2};
}

template<typename Dut_container>
Time<Scale::UTC> TimeConverter<Dut_container>::UT1_UTC(const Time<Scale::UT1> &Ut1) const noexcept {
    double utc1;
    double utc2;
    int func_value = iauUt1utc(Ut1.jdInt(), Ut1.jdFrac(), dutContainer_.dut(Ut1.mjd()), &utc1, &utc2);
    assert(func_value == -1);
    return {utc1, utc2};
}

template<typename Dut_container>
Time<Scale::UTC> TimeConverter<Dut_container>::TT_UTC(const Time<Scale::TT> &Tt) const noexcept {
    Time<Scale::TAI> timeTAI = TT_TAI(Tt);
    return TAI_UTС(timeTAI);
}

template<typename Dut_container>
Time<Scale::UTC> TimeConverter<Dut_container>::TCG_UTC(const TimeConverter::TCG &Tcg) const noexcept {
    Time<Scale::TT> timeTT = TCG_TT(Tcg);
    return TT_UTC(timeTT);
}

template<typename Dut_container>
Time<Scale::UTC> TimeConverter<Dut_container>::TCB_UTC(const TimeConverter::TCB &Tcb) const noexcept {
    Time<Scale::TT> timeTT = TCB_TT(Tcb);
    return TT_UTC(timeTT);
}

template<typename Dut_container>
Time<Scale::UTC> TimeConverter<Dut_container>::TDB_UTC(const TimeConverter::TDB &Tdb) const noexcept {
    Time<Scale::TT> timeTT = TDB_TT(Tdb);
    return TT_UTC(timeTT);
}

//----------------------------//
//------------UT1-------------//
//----------------------------//

template<typename Dut_container>
Time<Scale::UT1> TimeConverter<Dut_container>::TAI_UT1(const Time<Scale::TAI> &Tai) const noexcept {
    Time<Scale::TT> timeTT = TAI_TT(Tai);
    return TT_UT1(timeTT);
}

template<typename Dut_container>
Time<Scale::UT1> TimeConverter<Dut_container>::UTC_UT1(const Time<Scale::UTC> &Utc) const noexcept {
    double ut11;
    double ut12;
    int func_value = iauUtcut1(Utc.jdInt(), Utc.jdFrac(), dutContainer_.dut(Utc.mjd()), &ut11, &ut12);
    assert(func_value == -1);
    return {ut11, ut12};
}

template<typename Dut_container>
Time<Scale::UT1> TimeConverter<Dut_container>::TT_UT1(const Time<Scale::TT> &Tt) const noexcept {
    Time<Scale::UTC> timeUTC = TT_UTC(Tt);
    return UTC_UT1(timeUTC);
}

template<typename Dut_container>
Time<Scale::UT1> TimeConverter<Dut_container>::TCG_UT1(const TimeConverter::TCG &Tcg) const noexcept {
    Time<Scale::TT> timeTT = TCG_TT(Tcg);
    return TT_UT1(timeTT);
}

template<typename Dut_container>
Time<Scale::UT1> TimeConverter<Dut_container>::TDB_UT1(const TimeConverter::TDB &Tdb) const noexcept {
    Time<Scale::TT> timeTT = TDB_TT(Tdb);
    return TT_UT1(timeTT);
}

template<typename Dut_container>
Time<Scale::UT1> TimeConverter<Dut_container>::TCB_UT1(const TimeConverter::TCB &Tcb) const noexcept {
    Time<Scale::TT> timeTT = TCB_TT(Tcb);
    return TT_UT1(timeTT);
}


//----------------------------//
//------------TCG-------------//
//----------------------------//

template<typename Dut_container>
Time<Scale::TCG> TimeConverter<Dut_container>::TDB_TCG(const TimeConverter::TDB &Tdb) const noexcept {
    Time<Scale::TT> timeTT = TDB_TT(Tdb);
    return TT_TCG(timeTT);
}

template<typename Dut_container>
Time<Scale::TCG> TimeConverter<Dut_container>::TT_TCG(const TimeConverter::TT &Tt) const noexcept {
    double tcg1;
    double tcg2;
    int func_value = iauTttcg(Tt.jdInt(), Tt.jdFrac(), &tcg1, &tcg2);
    return {tcg1, tcg2};
}

template<typename Dut_container>
Time<Scale::TCG> TimeConverter<Dut_container>::TCB_TCG(const TimeConverter::TCB &Tcb) const noexcept {
    Time<Scale::TDB> timeTDB = TCB_TDB(Tcb);
    return TDB_TCG(timeTDB);
}

template<typename Dut_container>
Time<Scale::TCG> TimeConverter<Dut_container>::TAI_TCG(const TimeConverter::TAI &Tai) const noexcept {
    Time<Scale::TT> timeTT = TAI_TT(Tai);
    return TT_TCG(timeTT);
}

template<typename Dut_container>
Time<Scale::TCG> TimeConverter<Dut_container>::UTC_TCG(const TimeConverter::UTC &Utc) const noexcept {
    Time<Scale::TT> timeTT = UTC_TT(Utc);
    return TT_TCG(timeTT);
}

template<typename Dut_container>
Time<Scale::TCG> TimeConverter<Dut_container>::UT1_TCG(const TimeConverter::UT1 &Ut1) const noexcept {
    Time<Scale::TT> timeTT = UT1_TT(Ut1);
    return TT_TCG(timeTT);
}

//----------------------------//
//------------TDB-------------//
//----------------------------//

template<typename Dut_container>
Time<Scale::TDB> TimeConverter<Dut_container>::TT_TDB(const TimeConverter::TT &Tt) const noexcept {
    double tdb1;
    double tdb2;
    int func_value = iauTttdb(Tt.jdInt(), Tt.jdFrac(), dtr(Tt.jd()), &tdb1, &tdb2);
    return {tdb1, tdb2};
}

template<typename Dut_container>
Time<Scale::TDB> TimeConverter<Dut_container>::TCB_TDB(const TimeConverter::TCB &Tcb) const noexcept {
    double tdb1;
    double tdb2;
    int func_value = iauTcbtdb(Tcb.jdInt(), Tcb.jdFrac(), &tdb1, &tdb2);
    return {tdb1, tdb2};
}

template<typename Dut_container>
Time<Scale::TDB> TimeConverter<Dut_container>::TCG_TDB(const TimeConverter::TCG &Tcg) const noexcept {
    Time<Scale::TT> timeTT = TCG_TT(Tcg);
    return TT_TDB(timeTT);
}

template<typename Dut_container>
Time<Scale::TDB> TimeConverter<Dut_container>::TAI_TDB(const TimeConverter::TAI &Tai) const noexcept {
    Time<Scale::TT> timeTT = TAI_TT(Tai);
    return TT_TDB(timeTT);
}

template<typename Dut_container>
Time<Scale::TDB> TimeConverter<Dut_container>::UTC_TDB(const TimeConverter::UTC &Utc) const noexcept {
    Time<Scale::TT> timeTT = UTC_TT(Utc);
    return TT_TDB(timeTT);
}

template<typename Dut_container>
Time<Scale::TDB> TimeConverter<Dut_container>::UT1_TDB(const TimeConverter::UT1 &Ut1) const noexcept {
    Time<Scale::TT> timeTT = UT1_TT(Ut1);
    return TT_TDB(timeTT);
}

//----------------------------//
//------------TCB-------------//
//----------------------------//

template<typename Dut_container>
Time<Scale::TCB> TimeConverter<Dut_container>::TDB_TCB(const TimeConverter::TDB &Tdb) const noexcept {
    double tcb1;
    double tcb2;
    int func_value = iauTdbtcb(Tdb.jdInt(), Tdb.jdFrac(), &tcb1, &tcb2);
    return {tcb1, tcb2};
}

template<typename Dut_container>
Time<Scale::TCB> TimeConverter<Dut_container>::TT_TCB(const TimeConverter::TT &Tt) const noexcept {
    Time<Scale::TDB> timeTDB = TT_TDB(Tt);
    return TDB_TCB(timeTDB);
}

template<typename Dut_container>
Time<Scale::TCB> TimeConverter<Dut_container>::TCG_TCB(const TimeConverter::TCG &Tcg) const noexcept {
    Time<Scale::TDB> timeTDB = TCG_TDB(Tcg);
    return TDB_TCB(timeTDB);
}

template<typename Dut_container>
Time<Scale::TCB> TimeConverter<Dut_container>::TAI_TCB(const TimeConverter::TAI &Tai) const noexcept {
    Time<Scale::TT> timeTT = TAI_TT(Tai);
    return TT_TCB(timeTT);
}

template<typename Dut_container>
Time<Scale::TCB> TimeConverter<Dut_container>::UTC_TCB(const TimeConverter::UTC &Utc) const noexcept {
    Time<Scale::TT> timeTT = UTC_TT(Utc);
    return TT_TCB(timeTT);
}

template<typename Dut_container>
Time<Scale::TCB> TimeConverter<Dut_container>::UT1_TCB(const TimeConverter::UT1 &Ut1) const noexcept {
    Time<Scale::TT> timeTT = UT1_TT(Ut1);
    return TT_TCB(timeTT);
}

#endif //PROJECT_B_CONVERTER_H