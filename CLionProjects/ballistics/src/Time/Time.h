//
// Created by perseverance on 22.09.23.
//

#ifndef PROJCET_B_TIME_H
#define PROJCET_B_TIME_H

#include <iostream>
#include <cassert>
#include <array>
#include "sofa.h"


enum class Scale {
    TAI = 0, UTC = 1, UT1 = 2, TT = 3, TCG = 4, TCB = 5, TDB = 6
};

// что это такое
inline constexpr std::string_view UT1 = "ut1";
inline constexpr std::string_view UTC = "UTC";
inline constexpr std::string_view TAI = "TAI";
inline constexpr std::string_view TT = "tt";
inline constexpr std::string_view TCG = "tcg";
inline constexpr std::string_view TCB = "tcb";
inline constexpr std::string_view TDB = "tdb";
inline constexpr std::array<std::string_view, 7> SOFA_SCALES = {UT1, UTC, TAI, TT, TCG, TCB, TDB};

struct schedule {
    int year;
    int month;
    int day;
    int hour;
    int min;
    double sec;
};

//// Time class
template<Scale scale>
class Time {
private:
    double jdInt_;
    double jdFrac_;
    const double delta_jd_mjd = 2400000.5;
public:
    Time(double jd1 = 0, double jd2 = 0) noexcept;

    Time static buildFromJD(double jd) noexcept;

    /**
     *
     * @param mjd mjd
     * @return
     */
    [[nodiscard]] Time static buildFromMJD(double mjd) noexcept;

    [[nodiscard]] Time static buildFromCalendar(schedule sched);

    [[nodiscard]] double mjd() const noexcept { return jdInt_ + jdFrac_ - delta_jd_mjd; }

    [[nodiscard]] double jd() const noexcept { return jdInt_ + jdFrac_; }

    Time operator+(double seconds) const noexcept;

    Time operator-(double seconds) const noexcept;

    double const jdInt() const noexcept {
        return jdInt_;
    }

    double const jdFrac() const noexcept {
        return jdFrac_;
    }
};

//// Time functions
template<Scale scale>
Time<scale>::Time(double jd1, double jd2) noexcept {
    const int jd1Floor = jd1 >= 0 ? static_cast<int>(jd1) : static_cast<int>(jd1) - 1;
    const int jd2Floor = jd2 >= 0 ? static_cast<int>(jd2) : static_cast<int>(jd2) - 1;
    jdInt_ = jd1Floor + jd2Floor;

    const double delta1 = jd1 - jd1Floor;
    const double delta2 = jd2 - jd2Floor;
    jdFrac_ = delta1 + delta2;
    if (jdFrac_ >= 1) {
        jdInt_ += static_cast<int>(jdFrac_);
        jdFrac_ -= 1;
    }
}

template<Scale scale>
Time<scale> Time<scale>::buildFromJD(double jd) noexcept {
    return Time(jd, 0);
}

template<Scale scale>
Time<scale> Time<scale>::buildFromMJD(double mjd) noexcept {
    return Time(mjd + 2400000.5, 0);
}


template<Scale scale>
Time<scale> Time<scale>::buildFromCalendar(schedule sched) {
    double jd1;
    double jd2;
    const int out = iauDtf2d(static_cast<std::string>(SOFA_SCALES[static_cast<int>(scale)]).c_str(),
                             sched.year,sched.month, sched.day,
                             sched.hour, sched.min, sched.sec, &jd1, &jd2);
    assert(out < 0);
    return {jd1, jd2};
}

#endif //PROJCET_B_TIME_H