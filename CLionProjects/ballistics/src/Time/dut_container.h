//
// Created by perseverance on 15.10.23.
//

#ifndef STUDY_DUT_CONTAINER_H
#define STUDY_DUT_CONTAINER_H

#include <vector>

class Dut_container {
private:
    std::vector<std::pair<double, double>> dut_container_;
    double h;
    double start;
    double end;
public:
    explicit Dut_container(const std::vector<double> &mjd, const std::vector<double> &dut) noexcept;

    double dut(double mjd) const;
};


Dut_container::Dut_container(const std::vector<double> &mjd, const std::vector<double> &dut) noexcept {
    h = mjd[1] - mjd[0];
    start = mjd[0];
    end = mjd[mjd.size() - 1];

    dut_container_.resize(mjd.size() - 1);
    for (auto i = 0; i < mjd.size() - 1; i++) {
        dut_container_[i] = {mjd[i], (dut[i + 1] - dut[i]) / h};
    }
}

double Dut_container::dut(double mjd) const {
    if (mjd >= start && mjd <= end) {
        int step = static_cast<int>((mjd - start) / h);
        return dut_container_[step].first + (dut_container_[step].second) * h * static_cast<double>(step);
    }
}

#endif //STUDY_DUT_CONTAINER_H