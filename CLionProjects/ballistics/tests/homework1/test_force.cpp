//
// Created by perseverance on 19.10.23.
//

#include "gtest/gtest.h"
#include "homework1/delta_acceleration.cpp"
#include <fstream>

TEST(metric, metric) {
    Ballistics::Vector3d a = {1, 2, 3};
    Ballistics::Vector3d b = {2, 4, 5};

    double norm = metric(a, b);
    std::cout << a.size() << std::endl;
    for (auto i = 0; i < a.size(); i++) {
        ASSERT_NEAR(norm, 3, 1e-16);
    }
}

TEST(linspace, linspace) {
    std::vector<double> a = linspace(0, 99, 100);
}

TEST(sphere, sphere) {
    sphere_cont sphere1 = sphere(1, 10);

}

TEST(force, acc_interpolate1) {
    std::string model = "egm96";
    std::string path = "/home/perseverance/Desktop/ballistic_junior/egm96/gravity";

    const std::vector<double> Radius = linspace(10000000, 42000000, 26);
    std::vector<double> lg_l;
    lg_l.reserve(Radius.size());
    for (int i = 0; i < Radius.size(); i++) {
        lg_l[i] = calc_lg_L(model, path, Radius[i], 300, 128, 128,
                            64, 64);
    }

    std::ofstream out;
    out.open("/home/perseverance/Desktop/ballistic_junior/homework1/with_egm96/l128_64.txt");
    if (out.is_open()) {
        for (int j = 0; j < Radius.size(); ++j) {
            out << Radius[j] << " " << lg_l[j] << std::endl;
        }
    }
    out.close();
    std::cout << "File has been written" << std::endl;
}


TEST(force, acc_interpolate2) {
    std::string model = "egm2008";
    std::string path = "/home/perseverance/Desktop/ballistic_junior/egm2008/gravity";

    const std::vector<double> Radius = linspace(10000000, 42000000, 26);
    std::vector<double> lg_l;
    lg_l.reserve(Radius.size());
    for (int i = 0; i < Radius.size(); i++) {
        lg_l[i] = calc_lg_L(model, path, Radius[i], 300, 128, 128,
                            64, 64);
    }

    std::ofstream out;
    out.open("/home/perseverance/Desktop/ballistic_junior/homework1/"
             "with_egm2008/egm_2008_l_128_64.txt");
    if (out.is_open()) {
        for (int j = 0; j < Radius.size(); ++j) {
            out << Radius[j] << " " << lg_l[j] << std::endl;
        }
    }
    out.close();
    std::cout << "File has been written" << std::endl;
}

TEST(geo, geo) {
    Ballistics::Vector3d x = {0, 0, 40000000};
    Ballistics::Force::EarthGravityForce model1("egm96",
                                                "/home/perseverance/Desktop/ballistic_junior/egm96/gravity",
                                                128, 128);
    Ballistics::Vector3d acc = model1.calcAccelerationEcef(x);

    std::cout << acc.x() << std::endl;
    std::cout << acc.y() << std::endl;
    std::cout << acc.z() << std::endl;
}

TEST(a, a){
    auto a = 1e-16;
    std::cout << log10(a);
}