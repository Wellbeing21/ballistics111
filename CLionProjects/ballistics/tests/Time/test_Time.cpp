//
// Created by perseverance on 14.10.23.
//

#include <iostream>
#include "gtest/gtest.h"
#include "Time/converter.h"

TEST(time, jd){
    double jdint1 = 1000000000.5;
    double jdfrac1 = -1000000000.1;

    double jdint2 = 400.8;
    double jdfrac2 = 500.7;

    Time<Scale::TAI> timeTAI(jdint1, jdfrac1);
    Time<Scale::TT> timeTT(jdint2, jdfrac2);

    ASSERT_NEAR(timeTAI.jdInt(), 0, 1e-15);
    ASSERT_NEAR(timeTAI.jdFrac(), 0.4, 1e-7);
    ASSERT_NEAR(timeTT.jdInt(), 901, 1e-15);
    ASSERT_NEAR(timeTT.jdFrac(), 0.5, 1e-15);
}

TEST(time, mjd){
    double mjdint1 = 50000.5;
    Time<Scale::TAI> timeTAI = Time<Scale::TAI>::buildFromMJD(mjdint1);
    ASSERT_NEAR(timeTAI.jdInt(), 2450001, 1e-15);
    ASSERT_NEAR(timeTAI.jdFrac(), 0, 1e-15);
}

TEST(time, from_sched){}

TEST(time, to_sched){}