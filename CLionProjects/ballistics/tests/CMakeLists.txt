cmake_minimum_required(VERSION 3.24.2)
project(tests)

set(CMAKE_CXX_STANDART 20)

find_package(GTest REQUIRED)

add_executable(Google_Tests_run
        Time/test_Time.cpp
        Time/test_Time.cpp
        Converter/test_converter.cpp
        homework1/test_force.cpp
        Forces/AtmResistance.cpp
        Forces/SolarIrradiance.cpp
        Forces/EarthGravityForce.cpp
)

add_test(Google_Tests_run COMMAND Time/test_Time.cpp)

target_include_directories(Google_Tests_run PUBLIC ../src)
target_include_directories(Google_Tests_run PUBLIC ../src/Forces)

target_link_libraries(Google_Tests_run GTest::gtest_main ballistics)