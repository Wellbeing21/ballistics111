//
// Created by perseverance on 23.11.23.
//
#include "gtest/gtest.h"
#include "../Forces/solar_radiation/PenumbraShadow.h"
#include "../Forces/solar_radiation/IndependentShadow.h"

TEST(penumbra, first) {

    Ballistics::Vector3d object = {0, 0, 0};
    Ballistics::Vector3d body = {10, 0, 0};
    Ballistics::Vector3d sun = {20, 0, 0};

    Ballistics::Force::PenumbraShadow first(2, 0.5);
    double F = first.calcShadow(object, body, sun);

    ASSERT_NEAR(F, 0.75, 1e-3);
}

/// тест не сработает, земля внутри солнца
TEST(penumbra, second){
    Ballistics::Vector3d object = {0, 0, 0};
    Ballistics::Vector3d body = {9, 0, 0};
    Ballistics::Vector3d sun = {10, 0, 0};

    Ballistics::Force::PenumbraShadow first(1000, 2);
    double F = first.calcShadow(object, body, sun);
}

TEST(penumbra, third) {

    Ballistics::Vector3d object = {0, 0, 0};
    Ballistics::Vector3d body = {1, 0, 0};
    Ballistics::Vector3d sun = {2, 0, 0};

    Ballistics::Force::PenumbraShadow first(1, 0.5);
    double F = first.calcShadow(object, body, sun);

    ASSERT_NEAR(F, 0, 1e-16);
}

TEST(penumbra, fourth) {

    Ballistics::Vector3d object = {1, 0, 0};
    Ballistics::Vector3d body = {0, 0, 0};
    Ballistics::Vector3d sun = {2, 0, 0};

    Ballistics::Force::PenumbraShadow first(1, 0.5);
    double F = first.calcShadow(object, body, sun);

    ASSERT_NEAR(F, 1, 1e-16);
}

TEST(independent, first){
    Ballistics::Vector3d object = {-20000, 0, 0};
    Ballistics::Vector3d earth = {0, 0, 0};
    Ballistics::Vector3d moon = {0, 0, 0};
    Ballistics::Vector3d sun = {80000, 0, 0};

    double earth_rad = 2000;
    double moon_rad = 500;
    double sun_rad = 30000;
    Ballistics::Force::PenumbraShadow earthShadow_(sun_rad, earth_rad);
    Ballistics::Force::PenumbraShadow moonShadow_(sun_rad, moon_rad);

    double F_es = earthShadow_.calcShadow(object, earth, sun);
    double F_ms = moonShadow_.calcShadow(object, moon, sun);

    Ballistics::Force::IndependentShadow independent(earthShadow_, moonShadow_);

    ASSERT_NEAR(std::min(F_ms, F_es), F_es, 1e-16);
}