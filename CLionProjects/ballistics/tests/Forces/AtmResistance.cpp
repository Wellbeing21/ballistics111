//
// Created by perseverance on 09.11.23.
//

#include "../src/Forces/Atmosphere/4401CalcDensity.h"
#include "../src/Forces/Atmosphere/Gost4401.h"
#include "gtest/gtest.h"

/// счатает плотность и сравнивает с известным значением в таблице плотностей
TEST(density_interpoly, first){
    double h1 = 81600;
    double h2 = 81500;
    auto b = Ballistcs::Atmosphere::calc_density(h2);
    ASSERT_NEAR(b, 1.45408e-05, 1e-16);
}

TEST(density_interpoly, second){
    double h = 1200000;
    auto b = Ballistcs::Atmosphere::calc_density(h);
    ASSERT_NEAR(1.95503e-15, b, 1e-16);
}