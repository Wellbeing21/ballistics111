project(sofa)

file(GLOB_RECURSE src *.c)

add_library(${PROJECT_NAME} STATIC ${src})

target_include_directories(${PROJECT_NAME} PUBLIC third_party)